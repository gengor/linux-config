# Linux-Config #

Help to set up Linux (and cygwin or Windows Neovim) environments quicker.

These configs are based on [Wes Doyle's
dotfiles](https://github.com/wesdoyle/dotfiles) and Vim configs by [Juan Pedro
Fisanotti](http://fisadev.github.io/fisa-vim-config/).  Got inspired by Wes'
[YouTube Video](https://youtu.be/SXdIGNsz2G0).

So most of the credit goes to them.

## How do I set this up? ##

Cool stuff to make your life easier: Oh My zsh by Robby Russell, Vim plug and
the responding patched fonts, to make the zsh git support look awesome!

Also you will need Emacs if you want to go for the Spacemacs part.

Install this:

1. vim (via apt-get or cygwin install)
2. zsh (via apt-get or cygwin install)
3. zsh Addon *oh-my-zsh*
    * https://github.com/robbyrussell/oh-my-zsh
4. cygwin only: python 3.6 (in addition to "standalone" python)
5. possibly install patched fonts for the symbols in zsh
    * get them at https://github.com/powerline/fonts
    * install font (on Linux you may use the setup script from the fonts repo)
    * configure font for your terminal
6. Install these configs and finish set up
    * `git clone https://bitbucket.org/gengor/linux-config.git`
    * run `./linux-config/.make.sh` from home dir (it will backup configs)
    * start __vim__ and it will install the needed plugins itself. (Backup your old .vim folder, if you have one.)
    * clone the __[spacemacs](http://spacemacs.org/)__ folder via `git clone https://github.com/syl20bnr/spacemacs ~/.emacs.d`
    * start __emacs__ and install the missing plugins
7. more color schemes for vim and zsh
    * https://github.com/baines/vim-colorscheme-thaumaturge (Vim theme)
    * https://github.com/caiogondim/bullet-train.zsh (zsh Theme)
    * https://github.com/devnul1/heman/blob/master/colors/heman.vim
    * https://github.com/NLKNguyen/papercolor-theme
8. Make last adjustments to suit your directory structure
    * `.emacs` will need to know your home paths (default is to my private Windows settings)
    * `.zshrc` will need to know your correct home path
    * For the SVN support, you must add the `prompt_svn` to the agnostic theme.

If you want to use Neovim in Windows, just place the `init.vim` and `ginit.vim`
in your `~\AppData\Local\nvim` folder. You may need to make adjustments in the
`init.vim` to meet your directory structure.

## Screenshots (Ubuntu 17.4) ##

![picture](screenshot.png)
![picture](screenshot2.png)

* Font used: Inconsolata Medium
* Color Scheme zsh __agnoster__
* Color Scheme vim __PaperColor__

## How to use / customize

You can add a `.bash_aliases` or `.zsh_aliases` to your home and it will be
loaded. That way it is possible to add system individual stuff (like changing
to special directories) without interfereing with the repository version.

For some other stuff you will need to modify the `.zshrc` as it loads your path
dynamically.

## Additional stuff ##

There's an dump of my Gnome Terminal settings. You can import it by calling 
the respective script.

Dumpfile: ´gnome-terminal-dconf.dump´
Import script: ´install-gnome-terminal-config.sh´

_I'll be honst ... I have never tried that script. So, are you feeling lucky today?_

## Who do I talk to? ##

* Contact me at martin.palmowski@gmail.com
