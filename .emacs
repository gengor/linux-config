;; emacs config by Martin Palmowski, mostly salvaged from the internet! Kudos to those people!
;; Default Coding
(set-default-coding-systems 'utf-8)
(prefer-coding-system 'utf-8)

(require 'package)
(package-initialize)

(add-to-list 'package-archives
	     '("melpa-stable" . "https://stable.melpa.org/packages/") t)

;;(add-to-list 'package-archives
;;             '("marmalade" . "http://marmalade-repo.org/packages/") t)

(package-refresh-contents)

(defun install-if-needed (package)
  (unless (package-installed-p package)
    (package-install package)))

;; make more packages available with the package installer
(setq to-install
      '(magit yasnippet jedi auto-complete autopair flycheck python-mode markdown-mode org-jira))

(mapc 'install-if-needed to-install)

(require 'magit)
(global-set-key "\C-xg" 'magit-status)

(require 'auto-complete)
(require 'autopair)
(autopair-global-mode) ;; to enable in all buffers
(require 'yasnippet)
(require 'flycheck)
(global-flycheck-mode t)

; auto-complete mode extra settings
(setq
 ac-auto-start 2
 ac-override-local-map nil
 ac-use-menu-map t
 ac-candidate-limit 20)


;; Backup-Settings
(setq backup-directory-alist `(("." . "~/.saves")))
(setq backup-by-copying t)

;; Manually load python mode, as it was not in the packages.
;; ========== Python Mode ==========
;;(add-to-list 'load-path "~/.emacs.d/elpa/python-mode-6.2.3")
;;(require 'python-mode)

;; ;; Python mode settings
(require 'python-mode)
(add-to-list 'auto-mode-alist '("\\.py$" . python-mode))
(setq py-electric-colon-active t)
(add-hook 'python-mode-hook 'autopair-mode)
(add-hook 'python-mode-hook 'yas-minor-mode)

;; ;; Jedi settings
(require 'jedi)
;; It's also required to run "pip install --user jedi" and "pip
;; install --user epc" to get the Python side of the library work
;; correctly.
;; With the same interpreter you're using.

;; if you need to change your python intepreter, if you want to change it
;; (setq jedi:server-command
;;       '("python2" "/home/andrea/.emacs.d/elpa/jedi-0.1.2/jediepcserver.py"))

(add-hook 'python-mode-hook
	  (lambda ()
	    (jedi:setup)
	    (jedi:ac-setup)
            (local-set-key "\C-cd" 'jedi:show-doc)
            (local-set-key (kbd "M-SPC") 'jedi:complete)
            (local-set-key (kbd "M-.") 'jedi:goto-definition)))


(add-hook 'python-mode-hook 'auto-complete-mode)

;; auctex
;;(load "auctex.el" nil t t)
;;(load "preview-latex.el" nil t t)

(ido-mode t)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-faces-vector
   [default default default italic underline success warning error])
 '(ansi-color-names-vector
   ["#212526" "#ff4b4b" "#b4fa70" "#fce94f" "#729fcf" "#e090d7" "#8cc4ff" "#eeeeec"])
 '(custom-enabled-themes (quote (deeper-blue)))
 '(org-agenda-files
   (quote
    ("/cygdrive/c/Users/palm07/Documents/Sync/00 D1CS/PM-Coaching/Arvato-Aufgaben.org")))
 '(package-selected-packages
   (quote
    (org-jira python-mode magit jedi flycheck elpy autopair))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )


;; Org-Mode-Settings
;; Ideen von: http://www.hsbox.de/docs/emacs-orgmode/

(setq org-hide-leading-stars 'hidestars)

;; Capture mit "C-c c" oder F9
(define-key global-map "\C-cc" 'org-capture)
(define-key global-map (kbd "<f9>") 'org-capture)
(progn
  (setq org-capture-templates
	'(("t" "Aufgabe" entry (file+headline "/cygdrive/c/Users/palm07/Documents/Sync/00 D1CS/PM-Coaching/Arvato-Aufgaben.org" "Inbox")
	   "* TODO %?")
	  ("z" "Zeiteintrag in Aufgaben-privat.org" entry (file+headline "/cygdrive/c/Users/palm07/Documents/Sync/00 D1CS/PM-Coaching/Arvato-Aufgaben.org" "Inbox")
	   "* ZKTO %? \n  %i" :clock-in t :clock-resume t)
	  ("j" "Journal" entry (file+datetree "/cygdrive/c/Users/palm07/Documents/Sync/00\ D1CS/PM-Coaching/Arvato-Journal.org")
	   "* %?\nEntered on %U\n  %i"))))


(setq org-drawers (quote ("PROPERTIES" "CLOCKTABLE" "LOGBOOK" "CLOCK")))

;; Ein "!" bedeutet Zeitstempel
;; Ein "@" bedeutet Notiz
(setq org-todo-keywords
 '((sequence "TODO(t)" "APPT(a)" "PROJ(p)" "STARTED(s!)" "WAITING(w@/!)"
             "DELEGATED(g@/!)" "|" "DONE(d!)" "CANCELLED(c@)")))

;; Farben anpassen
(setq org-todo-keyword-faces
      '(("TODO"  . (:foreground "#ff79c6" :weight bold))
;;      ("TODO"  . (:foreground "#f87575" :weight bold))
        ("STARTED"  . (:foreground "#f7aade" :weight bold))
;;        ("APPT"  . (:foreground "sienna" :weight bold))
        ("APPT"  . (:foreground "#8be9fd" :weight bold))
        ("PROJ"  . (:foreground "#f1fa8c" :weight bold))
        ("WAITING"  . (:foreground "#ff555" :weight bold))
        ("DONE"  . (:foreground "#50fa7b" :weight bold))
        ("DELEGATED"  . (:foreground "#bfbfbf" :weight bold))
        ("CANCELLED"  . shadow)))

;; Fast TODO Selection
(setq org-use-fast-todo-selection t)

;; Einen Zeitstempel eintragen, wenn eine Aufgabe als erledigt markiert wird
(setq org-log-done 'time)

;; Einen eigenen Drawer benutzen
(setq org-log-into-drawer t)

;; deutsch as export language
(setq org-export-default-language "de")

;; deutscher Kalender:
(setq calendar-week-start-day 1
      calendar-day-name-array
        ["Sonntag" "Montag" "Dienstag" "Mittwoch"
         "Donnerstag" "Freitag" "Samstag"]
      calendar-month-name-array
        ["Januar" "Februar" "März" "April" "Mai"
         "Juni" "Juli" "August" "September"
	 "Oktober" "November" "Dezember"])

;; Agenda auch mit F19 (große Mac-Tastatur ganz rechts) aufrufen:
;; Per SSH muss 'C-c a' verwendet werden.
(global-set-key (kbd "<f12>") 'org-agenda)

;; Save clock data and notes in a separate drawer
(setq org-clock-into-drawer "CLOCK")

;; Clock in with F5:
(fset 'my-clock-in "\C-c\C-x\C-i")
(global-set-key (kbd "<f5>") 'my-clock-in)

;; Clock out with F8:
(fset 'my-clock-out "\C-c\C-x\C-o")
(global-set-key (kbd "<f8>") 'my-clock-out)

;; Resume clocking tasks when emacs is restarted
(org-clock-persistence-insinuate)

;; Yes it's long... but more is better ;)
(setq org-clock-history-length 35)

;; Resume clocking task on clock-in if the clock is open
(setq org-clock-in-resume t)

;; Change task state to STARTED when clocking in
;(setq org-clock-in-switch-to-state "STARTED")

;; Sometimes I change tasks I'm clocking quickly
;; this removes clocked tasks with 0:00 duration
;;(setq org-clock-out-remove-zero-time-clocks t)

;; Don't clock out when moving task to a done state
(setq org-clock-out-when-done nil)

;; Save the running clock and all clock history when exiting Emacs,
;; load it on startup
(setq org-clock-persist t)

;; Disable auto clock resolution
(setq org-clock-auto-clock-resolution nil)

(setq org-global-properties (quote (("Effort_ALL" .
				     "0:10 0:30 1:00 2:00 3:00 4:00 5:00 6:00 8:00"))))

(setq jiralib-url "https://jira.arvato-support.com")

(progn (find-file "/cygdrive/c/Users/palm07/Documents/Sync/00 D1CS/PM-Coaching/Arvato-Aufgaben.org"))



