" Martin Palmowski init.vim for neovim, based on stuff from Wes Doyle and Juan
" Pedro Fisanotti. I'm truly amazed how much customization is possible and how
" much these pleople actually put in to it.
"
" Load vim-plug

let vim_plug_just_installed = 0
let vim_plug_path = expand('C:\Users\palm07\AppData\Local\nvim\autoload\plug.vim')
if !filereadable(vim_plug_path)
    echo "Installing Vim-plug..."
    echo ""
    silent !mkdir -p C:\Users\palm07\AppData\Local\nvim\autoload
    silent !curl -fLo C:\Users\palm07\AppData\Local\nvim\autoload\plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
        let vim_plug_just_installed = 1
endif

" manually load vim-plug the first time
if vim_plug_just_installed
    :execute 'source '.fnameescape(vim_plug_path)
endif
set nocompatible
syntax on

filetype off


call plug#begin('C:\Users\palm07\AppData\Local\nvim\plugged')

" Plugins from github repos:
"=== Eye Candy ===
Plug 'NLKNguyen/papercolor-theme'
Plug 'dracula/vim'
Plug 'flazz/vim-colorschemes'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
" Paint css colors with the real color
Plug 'lilydjwg/colorizer'

"=== Snippets ===
" Plug 'SirVer/ultisnips'

"=== Code ===
Plug 'scrooloose/nerdtree'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'scrooloose/syntastic'
Plug 'scrooloose/nerdcommenter'
Plug 'raimondi/delimitmate'
Plug 'nvie/vim-flake8'
Plug 'davidhalter/jedi-vim'
" Class/module browser
" CPlug 'majutsushi/tagbar'

"=== Efficiency ===
" Snippets
if has('nvim')
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
else
  Plug 'Shougo/deoplete.nvim'
  Plug 'roxma/nvim-yarp'
  Plug 'roxma/vim-hug-neovim-rpc'
endif

" Shortcuts to code
" Plug 'mattn/emmet-vim'
" Surround objects with text
Plug 'tpope/vim-surround'
" Repeat plugin features with .
" Plug 'tpope/vim-repeat'
" Formatting content
Plug 'godlygeek/tabular'
Plug 'bronson/vim-trailing-whitespace'

"=== VCS ===
" Git/mercurial/others diff icons on the side of the file lines
Plug 'mhinz/vim-signify'
" Git support
Plug 'tpope/vim-fugitive'

"=== Writing ===
" Distraction free writing
Plug 'junegunn/goyo.vim'
" Light only active paragraph
Plug 'junegunn/limelight.vim'
" Markdown
Plug 'plasticboy/vim-markdown', { 'for': 'markdown' }
Plug 'dhruvasagar/vim-table-mode'

call plug#end()

filetype plugin indent on

"set term=screen-256color
colorscheme PaperColor

let python_highlight_all=1

set autoindent
set bg=light
set background=light
set backspace=indent,eol,start
set expandtab
set ignorecase
set incsearch
set laststatus=2
set linebreak
set nobackup
set noerrorbells
set nolist
set noswapfile
set novb
set wrap
set tw=80
set number
set relativenumber
set ruler
set scrolloff=15
set showmatch
set shiftwidth=2
set shortmess=I
set showcmd
set showmode
set sidescroll=1
set sidescrolloff=7
set smartcase
set softtabstop=2
set undolevels=1000

" Highlight colors for cursors and stuff
set cursorline
" insert mode"
" autocmd InsertEnter * highlight CursorLine guibg=DarkOrchid4
" visual and normal"
" autocmd InsertLeave * highlight CursorLine guibg=DarkSlateBlue
" highlight CursorLine guibg=DarkSlateGray
set colorcolumn=80
let &colorcolumn="80,".join(range(120,999),",")
" highlight ColorColumn ctermbg=233 guibg=Grey7

" Nicer colors
"highlight DiffAdd           cterm=bold ctermbg=none ctermfg=119
"highlight DiffDelete        cterm=bold ctermbg=none ctermfg=167
"highlight DiffChange        cterm=bold ctermbg=none ctermfg=227
"highlight SignifySignAdd    cterm=bold ctermbg=237  ctermfg=119
"highlight SignifySignDelete cterm=bold ctermbg=237  ctermfg=167
"highlight SignifySignChange cterm=bold ctermbg=237 ctermfg=227

" change the mapleader from \ to ,
let mapleader=","

" CtrlP
let g:ctrlp_map = '<c-p>'

" Deoplete Autocomplete stuff
let g:deoplete#enable_at_startup = 1

" Snippet Configs
" Plugin key-mappings.
" Note: It must be "imap" and "smap".  It uses <Plug> mappings.
"imap <C-k>     <Plug>(neosnippet_expand_or_jump)
"smap <C-k>     <Plug>(neosnippet_expand_or_jump)
"xmap <C-k>     <Plug>(neosnippet_expand_target)

" SuperTab like snippets behavior.
" Note: It must be "imap" and "smap".  It uses <Plug> mappings.
"imap <expr><TAB>
"\ pumvisible() ? "\<C-n>" :
"\ neosnippet#expandable_or_jumpable() ?
"\ "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"
"smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
"\ "\<Plug>(neosnippet_expand_or_jump)" : "\<TAB>"

" For conceal markers.
if has('conceal')
  set conceallevel=2 concealcursor=n
endif

" Enable snipMate compatibility feature.
let g:neosnippet#enable_snipmate_compatibility = 1

" Tell Neosnippet about the other snippets
let g:neosnippet#snippets_directory='~/.vim/bundle/vim-snippets/snippets'

"Airline
"let g:airline_theme='PaperColor'
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1

"NERDTree
map <C-n> :NERDTreeToggle<CR>

"Gvim mods
set encoding=utf-8
set hidden
set history=100
set mouse=a

set guioptions-=m
set guioptions-=T
set guioptions-=r
set guioptions-=Lo

"nerd-commenter settings
" Add spaces after comment delimiters by default
let g:NERDSpaceDelims = 1

" Use compact syntax for prettified multi-line comments
let g:NERDCompactSexyComs = 1

" Align line-wise comment delimiters flush left instead of following code
" indentation
let g:NERDDefaultAlign = 'left'

" Allow commenting and inverting empty lines (useful when commenting a
" region)
let g:NERDCommentEmptyLines = 1

" Enable trimming of trailing whitespace when uncommenting
let g:NERDTrimTrailingWhitespace = 1

" Switch in buffers more easily
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-h> <C-w>h
nnoremap <C-l> <C-w>l

" Remove all trailing whitespace by pressing F4
noremap <F4> :let _s=@/<Bar>:%s/\s\+$//e<Bar>:let @/=_s<Bar><CR>

" Tagbar --------------------------------
" toggle tagbar display
map <F5> :TagbarToggle<CR>
" autofocus on tagbar open
let g:tagbar_autofocus = 1

" Python run with F9
nnoremap <buffer> <F9> :exec '!python' shellescape(@%, 1)<cr>

" Jedi-vim ------------------------------
" All these mappings work only for python code:
" Go to definition
let g:jedi#goto_command = ',d'
" Find ocurrences
let g:jedi#usages_command = ',o'
" Find assignments
let g:jedi#goto_assignments_command = ',a'
" Go to definition in new tab
nmap ,D :tab split<CR>:call jedi#goto()<CR>

" Signify ------------------------------
" this first setting decides in which order try to guess your current vcs
" UPDATE it to reflect your preferences, it will speed up opening files
let g:signify_vcs_list = [ 'git' ]
" mappings to jump to changed blocks
nmap <leader>sn <plug>(signify-next-hunk)
nmap <leader>sp <plug>(signify-prev-hunk)

let g:table_mode_corner_corner='+'
let g:table_mode_header_fillchar='='

" Cursor Modes
let &t_ti.="\e[1 q"
let &t_SI.="\e[5 q"
let &t_EI.="\e[1 q"
let &t_te.="\e[0 q"

" Exit insert mode more elegantly
set timeoutlen=300
inoremap fj <Esc>l
inoremap jf <Esc>l

" more colors
set termguicolors
